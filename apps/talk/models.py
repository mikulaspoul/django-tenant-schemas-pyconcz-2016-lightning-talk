# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Talk(models.Model):

    name = models.CharField(verbose_name=_("Name"), max_length=100)
    author = models.CharField(verbose_name=_("Author"), max_length=30)
