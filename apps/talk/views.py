# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.urls import reverse_lazy
from django.views.generic.edit import CreateView

from apps.talk.models import Talk


class CreateTalkView(CreateView):

    model = Talk
    fields = "__all__"
    success_url = reverse_lazy("talks")

    def get_context_data(self, **kwargs):
        data = super(CreateTalkView, self).get_context_data(**kwargs)

        data["talks"] = Talk.objects.all()
        data["conference"] = self.request.tenant  # The active domains model instance

        return data
