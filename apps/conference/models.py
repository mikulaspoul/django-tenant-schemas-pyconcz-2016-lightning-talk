# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.db import models
from tenant_schemas.models import TenantMixin
from django.utils.translation import ugettext_lazy as _


class Conference(TenantMixin):  # TenantMixin instead of models.Model, implements creating schemas and extra arguments

    name = models.CharField(verbose_name=_("Name"), max_length=20)
