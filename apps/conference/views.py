# encoding=utf-8
from __future__ import unicode_literals, print_function

from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from tenant_schemas.utils import get_public_schema_name

from apps.conference.models import Conference


class CreateConferenceView(CreateView):

    model = Conference
    fields = ["name", "schema_name", "domain_url"]
    success_url = reverse_lazy("index")

    def get_context_data(self, **kwargs):
        data = super(CreateConferenceView, self).get_context_data(**kwargs)

        # there has to be tenant entry for the public domain, but it hasn't got any talk so we need to exclude it
        data["conferences"] = Conference.objects.exclude(schema_name=get_public_schema_name())

        return data
