"""
This urls file specifies urls for the public domain (just pycon.dev)
"""
from django.conf.urls import url
from apps.conference.views import CreateConferenceView

urlpatterns = [
    url(r'', CreateConferenceView.as_view(), name="index")
]
