"""
This urls file specifies urls for when you have the subdomain active (eg cz.pycon.dev)
"""
from django.conf.urls import url
from apps.talk.views import CreateTalkView

urlpatterns = [
    url(r'^$', CreateTalkView.as_view(), name="talks")
]
