"""
This is the basic django settings with some changes for the tenant schemas - I'll describe with comments
"""

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'f%wf7v+cq@va$d=73e&1t6j_3(sdns^u(@p%^350@k!)pu!12^'
DEBUG = True
ALLOWED_HOSTS = []


# This specifies which apps will be in the schema common for all the tenants
SHARED_APPS = (
    'tenant_schemas',  # this should be on top

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',

    'apps.conference',
)

# This specifies which apps will be created in the database for every specific tenant
TENANT_APPS = (
    'django.contrib.contenttypes',  # This has to be in every schema

    'apps.talk'
)

# but we also have to list all installed apps for the remaining django functionality
INSTALLED_APPS = list(SHARED_APPS) + [app for app in list(TENANT_APPS) if app not in list(SHARED_APPS)]


MIDDLEWARE = [
    # This middleware sets the SEARCH PATH to the schema_name from the model based on the subdomain
    'tenant_schemas.middleware.TenantMiddleware',

    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'urls'

# this is not obligatory, but you can create a different urls file for the public domain not specific for tenants
PUBLIC_SCHEMA_URLCONF = "urls_public"


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            "templates"
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',  # instead of 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dtslightning',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '',
        }
    }

TENANT_MODEL = "conference.Conference"  # which model should be used to look up the schema name based on the domain

# and we also have to set the database router
DATABASE_ROUTERS = (
    'tenant_schemas.routers.TenantSyncRouter',
)


LANGUAGE_CODE = 'en-gb'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True
